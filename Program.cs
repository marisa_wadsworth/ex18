﻿using System;

namespace ex_18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
        
            {
                //Declare Variables
                var num1 = 3; // this is an int
                var num2 = 6;
                var num3 = 5;
                var num4 = 2;
                var num5 = 9;
                var num6 = 12;
                //var I_am_thinking… = *3;

            
                Console.WriteLine($"The vaule of num1 ={num1} ");
                Console.WriteLine($"The vaule of num2 = {num2}");
                Console.WriteLine($"The vaule of num3 = {num3}");
                Console.WriteLine($"The vaule of num4 = {num4}");
                Console.WriteLine($"The vaule of num5 = {num5}");
                Console.WriteLine($"The vaule of num6 = {num6}");

                Console.WriteLine("");
                Console.WriteLine($" num1 + num2 = {(3 + 6)}");
                Console.WriteLine($" num3 + num4 = {(5 + 2)}");
                Console.WriteLine($" num5 + num6 = {(9 + 12)}");

                Console.WriteLine("");
                num1 = num1 + num2;
                Console.WriteLine($" num1 + num2 = {num1}");
                num3 = num3 + num4;
                Console.WriteLine($" num3 + num4 = {num3}");
                num6 = num5 + num6;
                Console.WriteLine($" num5 + num6 = {num6}");
                num1 = num1 + num6;
                Console.WriteLine($" num1 + num6 = {num1}");

                //Console.WriteLine("I I_am_thinking…");
                //Program does not work when you try to multiply the variable by 3.


                //End the program with blank line and instructions
                Console.ResetColor();
                Console.WriteLine();
                Console.WriteLine("Press <Enter> to quit the program");
                Console.ReadKey();
            }
        }
    }
}
